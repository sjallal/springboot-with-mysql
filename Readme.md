### SETUP AND RUN:
- Install Java 17
  - Check all the installed versions of Java in the system: `/usr/libexec/java_home -V`
  - Update `.zshrc` with the required version `export JAVA_HOME=$(/usr/libexec/java_home -v 18.0.2)`
- Install IntelliJ
- Install maven
- Go to Aiven, create a MySQL service
  - Get Host, Database Name, Port, User & Password details
![](aiven_mysql.png)
- Go to Spring Initialr, Create a Maven Project, Adde dependencies listed below
![](spring_initializr.png)
  - Click on GENERATE, download, unzip and open the project in IntelliJ
- Add db connection details in [application.properties](src/main/resources/application.properties)
- Add User Entity
- Add User Repository
- Add User Controller
- Run the application with command: `mvn spring-boot:run`
- Add a new user from terminal: `curl http://localhost:8080/user/add -d name="Saqib Jallal" -d email="saqib@xyz.com"`
- List all the users from terminal: `curl http://localhost:8080/user/all`